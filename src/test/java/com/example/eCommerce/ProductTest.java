package com.example.eCommerce;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProductTest {
    private String convertPOJOToJSONString(Product product) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(product);
    }

    private Product convertJSONStringToPOJO(String input) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(input, Product.class);
    }

    @Test
    void productToJson() throws JsonProcessingException {
        Product product = new Product(1, "book", 500, "Rupees",true);

        assertEquals("{\"productId\":1,\"productName\":\"book\",\"price\":500,\"availability\":true,\"type\":\"Rupees\"}", convertPOJOToJSONString(product));
    }
    @Test
    void jsonToProduct() throws IOException {
        Product product = new Product(1, "book", 500, "Rupees",true);

        assertEquals(product, convertJSONStringToPOJO("{\"productId\":1,\"productName\":\"book\",\"price\":500,\"availability\":true,\"type\":\"Rupees\"}"));
    }


}