package com.example.eCommerce;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductController.class)
class ProductControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ProductService productService;
    ObjectMapper objectMapper;


    @BeforeEach
    void setup() {
        objectMapper = new ObjectMapper();
    }

    @Nested
    class GetProduct {
        @Test
        void shouldGetProductSuccessfully() throws Exception {
            Mockito.when(productService.getProductDetails(1)).thenReturn(new Product(1,
                    "book", 500, "Rupees", true));

            mockMvc.perform(get("/products?productId=1"))
                    .andExpect(status().isOk())
                    .andExpect(content().json("{\"productId\":1, \"productName\":\"book\", \"price\":500, \"type\":\"Rupees\",\"availability\":true}"));

        }

        @Test
        void shouldThrowProductNotRegisteredExceptionForProductNotRegistered() throws Exception {
            Mockito.when(productService.getProductDetails(2)).thenThrow(ProductNotRegisteredException.class);

            mockMvc.perform(get("/products?productId=2"))
                    .andExpect(status().is4xxClientError());
        }
    }

    @Nested
    class CreateProduct {
        @Test
        void shouldCreateProduct() throws Exception {
            Mockito.when(productService.createProduct(new Product(1, "book", 500, "Rupees", true)))
                    .thenReturn(new Product(1, "book", 500, "Rupees", true));

            mockMvc.perform(post("/products").content(objectMapper.writeValueAsString(new Product(1, "book", 500, "Rupees", true)))
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isCreated())
                    .andExpect(content().json(objectMapper.writeValueAsString(new Product(1, "book", 500, "Rupees", true))));
        }
    }

    @Test
    void updateProduct() throws Exception {
        Mockito.when(productService.updateProduct(1, 1000, true))
                .thenReturn(new Product(1, "book", 1000, "Rupees", false));

        mockMvc.perform(patch("/products/1").content(objectMapper.writeValueAsString(new Product(1, "book", 1000, "Rupees", true)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(objectMapper.writeValueAsString(new Product(1, "book", 1000, "Rupees", false))));

    }

}
