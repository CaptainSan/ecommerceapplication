package com.example.eCommerce;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class ProductServiceTest {
    private ProductService productService;
    @Autowired
    private ProductRepository productRepository;

    @BeforeEach
    void setup() {
        productService = new ProductService(productRepository);
        productRepository.save(new Product(1, "book", 500, "Rupees",true));
        productRepository.save(new Product(2, "mobile", 5000, "Rupees",true));
    }

    @AfterEach
    void end(){
        productRepository.deleteAll();
    }

    @Nested
    class TestGetProduct {
        @Test
        void shouldReturnProductDetails() throws ProductNotRegisteredException {
            assertEquals(new Product(1, "book", 500, "Rupees",true), productService.getProductDetails(1));
        }

        @Test
        void shouldThrowProductNotRegisteredExceptionForProductNotRegistered() {
            assertThrows(ProductNotRegisteredException.class, () -> {
                productService.getProductDetails(3);
            });
        }
    }

    @Test
    void shouldCreateProduct() {
        assertEquals(new Product(3, "pen", 20, "Rupees",true), productService.createProduct(3, "pen", 20, "Rupees",true));
    }

}
