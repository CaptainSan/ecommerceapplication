package com.example.eCommerce;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping("/products")
@RestController
class ProductController {
    @Autowired
    private ProductService productService;

    @GetMapping
    Product getProduct(@RequestParam("productId") int productId) throws ProductNotRegisteredException {
        return productService.getProductDetails(productId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    Product createProduct(@Valid @RequestBody Product product) {
        return productService.createProduct(product);
    }

//    @PatchMapping("/{productId}")
//    Product updatePrice(@PathVariable int productId, @RequestBody Product updatedPrice) {
//        return productService.updatePrice(productId, updatedPrice.getPrice());
//    }

    @PatchMapping("/{productId}")
    Product updatePrice(@PathVariable int productId, @RequestBody Product updatedProduct) {
        return productService.updateProduct(productId, updatedProduct.getPrice(), updatedProduct.isAvailability());
    }
}
