package com.example.eCommerce;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Product Not Registered")
public class ProductNotRegisteredException extends RuntimeException {
}
