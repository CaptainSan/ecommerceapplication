package com.example.eCommerce;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeId;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Objects;

@Getter
@NoArgsConstructor
@Entity
@EqualsAndHashCode(exclude = {"id"})
public class Product {
    @JsonIgnore
    @Id
    @GeneratedValue
    Long id;

    private int productId;

    private String productName;

    private int price;

    private boolean availability;

    String type;

    Product(int productId, String productName, int price, String type, boolean availability) {
        this.productId = productId;
        this.productName = productName;
        this.price = price;
        this.type = type;
        this.availability = availability;
    }

    void changePrice(int updatedPrice) {
        this.price = updatedPrice;
    }

    void changeAvailability(boolean availability) {
        this.availability = availability;
    }


//    enum PriceType {
//        DOLLARS, RUPEES
//    }
}
