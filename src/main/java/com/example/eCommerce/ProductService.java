package com.example.eCommerce;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    Product createProduct(int productId, String productName, int productPrice, String type, boolean availability) {
        Product product = new Product(productId, productName, productPrice, type, availability);
        this.productRepository.save(product);
        return product;
    }

    Product createProduct(Product product) {
        this.productRepository.save(product);
        return product;
    }

    Product getProductDetails(int productId) throws ProductNotRegisteredException {
        Product product = this.productRepository.findByProductId(productId);
        if (product == null) {
            throw new ProductNotRegisteredException();
        }
        return product;
    }

    Product updatePrice(int productId, int updatedPrice) {
        Product product = getProductDetails(productId);
        product.changePrice(updatedPrice);
        productRepository.save(product);
        return product;
    }

//    Product updateAvailability(int productId, boolean availability) {
//        Product product = getProductDetails(productId);
//        product.changeAvailability(availability);
//        if(!availability){
//            productRepository.delete(product);
//            throw new ProductNotRegisteredException();
//        }
//        productRepository.save(product);
//        return product;
//    }

    Product updateProduct(int productId, int updatedPrice, boolean updatedAvailability) {
        Product product = getProductDetails(productId);
        product.changePrice(updatedPrice);
        product.changeAvailability(updatedAvailability);
        if(!updatedAvailability){
            productRepository.delete(product);
            throw new ProductNotRegisteredException();
        }
        productRepository.save(product);
        return product;
    }
}
